use yew::prelude::*;
use yew_router::prelude::*;

mod nav_menu;
mod pages;

use nav_menu::NavMenu;
use pages::*;

#[derive(Clone, Routable, PartialEq)]
enum Route {
    #[at("/")]
    Home,
    #[at("/counter")]
    Counter,
    #[at("/fetch-data")]
    FetchData,
    #[not_found]
    #[at("/error")]
    NotFound,
}

fn switch(routes: &Route) -> Html {
    match routes {
        Route::Home => html! { <Home /> },
        Route::Counter => html! { <Counter /> },
        Route::FetchData => html! { <FetchData /> },
        Route::NotFound => html! { <NotFound /> },
    }
}

pub fn now() -> js_sys::Date {
    js_sys::Date::new_0()
}

#[function_component(App)]
pub fn app() -> Html {
    let year = now().get_full_year();

    html! {
        <BrowserRouter>
            <NavMenu />

            <main>
                <div class="row col m-3">
                    <Switch<Route> render={ Switch::render(switch) } />
                </div>
            </main>
            <footer class="footer">
                <div class="container text-center text-muted">
                    { "©" } { year }
                </div>
            </footer>
        </BrowserRouter>
    }
}

fn main() {
    yew::start_app::<App>();
}
