use yew::{function_component, html, use_state, Callback};

#[function_component(Counter)]
pub fn counter() -> Html {
    let counter = use_state(|| 0);
    let onclick = {
        let counter = counter.clone();
        Callback::from(move |_| counter.set(*counter + 1))
    };

    html! {
        <div>
            <h1>{"Counter"}</h1>

            <p>{"This is a simple example of a Yew component."}</p>

            <p aria-live="polite">{"Current count: "}<strong>{{ *counter }}</strong></p>

            <button class="btn btn-primary" {onclick}>{ "Increment"}</button>
        </div>
    }
}
