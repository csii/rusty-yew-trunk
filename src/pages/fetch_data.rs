use js_sys::{Date, Math};
use yew::{function_component, html, html_nested, Properties};

fn get_random(min: i32, max: i32) -> i32 {
    Math::floor(Math::random() * (max - min) as f64 + min as f64) as i32
}

fn add_days(days: u32) -> Date {
    let date = Date::new_0();
    date.set_date(date.get_date() + days);

    date
}

#[derive(Properties, PartialEq, Clone)]
struct WeatherForcast {
    pub date: Date,
    pub temperature_c: i32,
    pub summary: Option<String>,
}

impl WeatherForcast {
    pub fn get_temperature_f(&self) -> i32 {
        32 + (self.temperature_c as f32 / 0.5556) as i32
    }
}

#[function_component(FetchData)]
pub fn fetch_data() -> Html {
    let summaries = vec![
        "Freezing",
        "Bracing",
        "Chilly",
        "Cool",
        "Mild",
        "Warm",
        "Balmy",
        "Hot",
        "Sweltering",
        "Scorching",
    ];

    let forcasts: Option<Vec<WeatherForcast>> = Some(
        (1..=5)
            .map(|index| WeatherForcast {
                date: add_days(index),
                temperature_c: get_random(-20, 55),
                summary: Some(
                    summaries[get_random(0, summaries.len() as i32) as usize].to_string(),
                ),
            })
            .collect(),
    );

    html! {
        <div>
            <h1 id="tableLabel">{"Weather forecast"}</h1>

            <p>{"This component demonstrates fetching data from the server."}<br />{"(Not Really)"}</p>
            {
            match forcasts {
                Some(forcast_data) => html! {
                    <table class="table table-striped" aria-labelledby="tableLabel">
                        <thead>
                            <tr>
                            <th>{"Date"}</th>
                            <th>{"Temp. (C)"}</th>
                            <th>{"Temp. (F)"}</th>
                            <th>{"Summary"}</th>
                            </tr>
                        </thead>
                        <tbody>
                        { for forcast_data.iter().map(|forecast| {
                            html_nested! {
                                <tr>
                                    <td>{ forecast.date.to_date_string() }</td>
                                    <td>{ forecast.temperature_c }</td>
                                    <td>{ forecast.get_temperature_f() }</td>
                                    <td>{ if forecast.summary.is_some() {forecast.summary.clone().unwrap()} else {String::from("")}}</td>
                                </tr>
                            }})
                        }
                        </tbody>
                    </table>
                },
                None => html! {<p><em>{"Loading..."}</em></p>},
            }
        }

        </div>
    }
}
