use yew::{classes, function_component, html};
use yew::{use_state, Callback};

use crate::Link;
use crate::Route;

#[function_component(NavMenu)]
pub fn nav_menu() -> Html {
    let is_expanded = use_state(|| false);

    let onclick = {
        let is_expanded = is_expanded.clone();
        Callback::from(move |_| is_expanded.set(!*is_expanded))
    };

    html! {
      <header>
        <nav class="navbar navbar-expand-sm navbar-toggleable-sm navbar-light bg-white border-bottom box-shadow mb-3">
        <div class="container-fluid">
            <Link<Route> classes={"navbar-brand"} to={Route::Home} >
                { "Rusty Yew Trunk" }
            </Link<Route>>

            <button class="navbar-toggler" type="button"
                aria-label="Toggle navigation" aria-expanded={is_expanded.to_string()}
            {onclick}><span class="navbar-toggler-icon"></span></button>

            <div class={classes!("collapse", "navbar-collapse", if *is_expanded { "show" } else {""} )}>
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <Link<Route> classes={classes!("nav-link")} to={Route::Home}>
                            { "Home" }
                        </Link<Route>>
                    </li>
                    <li class="nav-item">
                        <Link<Route> classes={classes!("nav-link")} to={Route::Counter}>
                            { "Counter" }
                        </Link<Route>>
                    </li>
                    <li class="nav-item">
                        <Link<Route> classes={classes!("nav-link")} to={Route::FetchData}>
                            { "Fetch Data" }
                        </Link<Route>>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            { "More" }
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li>
                                <Link<Route> classes={"dropdown-item"} to={Route::NotFound}>
                                    { "Page Not Found" }
                                </Link<Route>>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        </nav>
    </header>
    }
}
